# ad auth

## build

mvn clean package

## get secret

SPRING_SECURITY_OAUTH2_CLIENT_REGISTRATION_AZURE_CLIENT_SECRET=$(az keyvault secret show --vault-name connectivity992fd6ad --name iot-service-tool -o json | jq -r .value)

## run

mvn spring-boot:run